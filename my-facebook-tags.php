<?php

/**
 * Plugin Name: My Facebook Tags
 * Plugin URI: http://www.example.com/
 * Description: This plugin adds some Facebook Open Graph tags to our single posts.
 * Version: 1.0.0
 * Author: Rafael Figueredo
 * Author URI: http://www.example.com/
 * License: GPL2
 */



add_action( 'wp_head', 'my_facebook_tags' );


function my_facebook_tags() {
  if( is_single() ) {
  ?>
    <meta property="og:title" content="<?php the_title() ?>" />
    <meta property="og:site_name" content="<?php bloginfo( 'name' ) ?>" />
    <meta property="og:url" content="<?php the_permalink() ?>" />
    <meta property="og:description" content="<?php the_excerpt() ?>" />
    <meta property="og:type" content="article" />
    
    <?php 
      if ( has_post_thumbnail() ) :
        $image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large' ); 
    ?>
      <meta property="og:image" content="<?php echo $image[0]; ?>"/>  
    <?php endif; ?>
    
  <?php
  }
}

add_action('admin_menu', 'my_plugin_menu');

function my_plugin_menu() {
	add_menu_page('My Plugin Settings', 'My Plugin Settings', 'administrator', 'my-plugin-settings', 'my_plugin_settings_page', 'dashicons-admin-generic');
}

add_action( 'admin_init', 'my_plugin_settings' );

function my_plugin_settings() {
	register_setting( 'my-plugin-settings-group', 'accountant_name' );
	register_setting( 'my-plugin-settings-group', 'accountant_phone' );
	register_setting( 'my-plugin-settings-group', 'accountant_email' );
}

function my_plugin_settings_page() {
 
 ?>
  <div class="wrap">
  <h2>Staff Details</h2>

	<form method="post" action="options.php">
	    <?php settings_fields( 'my-plugin-settings-group' ); ?>
	    <?php do_settings_sections( 'my-plugin-settings-group' ); ?>
	    <table class="form-table">
	        <tr valign="top">
	        <th scope="row">Accountant Name</th>
	        <td><input type="text" name="accountant_name" value="<?php echo esc_attr( get_option('accountant_name') ); ?>" /></td>
	        </tr>
	         
	        <tr valign="top">
	        <th scope="row">Accountant Phone Number</th>
	        <td><input type="text" name="accountant_phone" value="<?php echo esc_attr( get_option('accountant_phone') ); ?>" /></td>
	        </tr>
	        
	        <tr valign="top">
	        <th scope="row">Accountant Email</th>
	        <td><input type="text" name="accountant_email" value="<?php echo esc_attr( get_option('accountant_email') ); ?>" /></td>
	        </tr>
	    </table>
	    
	    <?php submit_button(); ?>
	
	</form>
  </div>

  <?php
	
}